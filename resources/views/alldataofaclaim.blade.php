@extends('layouts.frontend.main')
@section('main-container')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Claim History </h2>
          <h2>Welcome {{$user->name}}</h2>
          <ol>
            <li><a href="index.html">Home</a></li>
            <li>Activites</li>
          </ol>
        </div>

        <div class="container mt-3">
      <div class="row">
      <h2>Health-Id : {{$activities->healthid}} <a href="{{route('edit',$id)}}" ><button class="btn btn-success">Edit</button></a></h2> 
      <table class="table table-bordered">

      <thead>
      <tr>
        
        <th>Name</th>
        <th>State</th>
        <th>District</th>
        <th>Ngo </th>
      </tr>
      @foreach($bdata as  $k=>$v)
      

      
     
      <td>{{$v}}</td>
      
     
      
      @endforeach
</table>
      <div class="col-md-8">
      <table class="table table-bordered">

      <thead>
      <tr>
        
        <th>Sl.No</th>
        <th>Descriptions</th>
        <th>Creator</th>
        <th>Date & Time </th>
      </tr>
      @foreach($activities->descriptions as $key => $value)
      <tr>

      <td> {{$c}}</td>
      <td>{{$value->description}}</td>
      <td>{{$activities->user->name}}</td>
      <td>{{date_format($value->created_at,'d/ M /Y')}}</td>
      </tr>
    
      @endforeach
    </thead>
    <tbody>
      </table>
</div>
<div class="col-md-4">
  @if(count($activities->documents)>0)
<table class="table table-bordered">

<thead>
<tr>
  
  <th>Sl.No</th>
  <th>Documents</th>
  <th>Creator</th>
   <th>Date & Time </th>
</tr>
@foreach($activities->documents as $key => $value)
<tr>

<td> {{$loop->iteration}}</td>
<td><a href="{{asset('storage/document/')}}/{{$value->documentpath}}" target="_blank"> Click Here</a></td>
<td>{{$activities->user->name}}</td>
      <td>{{date_format($value->created_at,'d/M/Y')}}</td>
</tr>

@endforeach
</thead>
<tbody>
</table>
@else 
<h3>No Documents Found</h3>
@endif
</div>
</div>
</div>
</section>


        @endsection