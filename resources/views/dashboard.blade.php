@extends('layouts.frontend.main')

@section('main-container')
<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Dashboard</h2>
          <h2>Welcome {{$user->name}}</h2>
          <ol>
            <li><a href="">Home</a></li>
            <li>Dashboard</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->

    <section class="inner-page">
      <div class="container">
      <a href="{{url('/addactivities')}}"><button type="button" class="btn btn-primary">Add Activity</button></a>
      <a href="{{url('/activities')}}"><button type="button" class="btn btn-primary">View Activity</button></a>
      
       <br><br><br><br>
      
      </div>
    </section>

  </main><!-- End #main -->
  @endsection