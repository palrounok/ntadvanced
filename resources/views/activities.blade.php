@extends('layouts.frontend.main')
@section('main-container')

<main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Activites</h2>
          <h2>Welcome {{$user->name}}</h2>
          <ol>
            <li><a href="index.html">Home</a></li>
            <li>Activites</li>
          </ol>
        </div>

      </div>
    </section><!-- End Breadcrumbs -->
    <div class="container mt-3">
      <div class="row">
      <h2>Claims</h2>
  <p>All claim datas are showing below with users who craete these claim datas</p>    
  <div class="col-md-6">
  <form action="{{route('search')}}" method="POST">
    @csrf
  <div class="mb-3 mt-3">
  
    <input type="text" class="form-control" id="search" placeholder="Search Health-Id" name="search">
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
<div class="row">
    <div class="col-md-6">
         
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Sl.no</th>
        <th>Health Id</th>
        <th>Beneficiary Details</th>
        <th>Claim Activity History</th>
      </tr>
    </thead>
    <tbody>
      @foreach($activities as $k=>$v)

      <tr> 
        <td>{{$loop->iteration}}</td>
<td>
{{$v->healthid}}
</td>
<td>
<a href="{{url('/onedataofaclaim',$v->healthid)}}">Click Here</a>
</td>
<td>
<a href="{{url('/alldataofaclaim',$v->id)}}"> Click Here</a>
</td>
</tr>
@endforeach
    </tbody>
  </table>
 
</div>
<div class="col-md-6"> 
@if($errors->any())
<h4 style="color:green">Health Id : {{$errors->first()}}</h4>
@endif
@if($errors->any())
    <table class="table table-bordered"><thead><tr><th>Health Id</th><th>Name</th><th>State</th><th>District</th><th>Ngo</th></tr></thead>
    <tbody><tr>
   
    
   @foreach ($errors->all() as $error)
      <td>{{ $error }}</td>
  @endforeach
  </tr>
  </tbody>
  </table>
@endif
</div>
</div>
</div>
</div>

  </main>
  <br>
@endsection
