@extends('layouts.frontend.main')
@section('main-container')
<main id="main">

<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
  <div class="container">

    <div class="d-flex justify-content-between align-items-center">
      <h2>Add Claim Details  </h2>
      <h2>Welcome {{$user->name}}</h2>
      <ol>
        <li><a href="index.html">Home</a></li>
        <li>Activites</li>
      </ol>
    </div>
    <div class = "row">
<div class="col-md-6">
    <!-- @if($errors->any())
    @if($errors->has('color') && $errors->has('color') == 'red') 
    <div class="alert alert-danger">
    <h4 style="color:red">{{$errors->first()}}</h4>
    </div>
    
    @else
    <div class="alert alert-success">
    <h4 style="color:green">{{$errors->first()}}</h4>
    </div>
    @endif
@endif --> 
 
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session()->has('msg'))
<div class="alert alert-success">
{{session()->get('msg')}}
</div>
@endif

</div></div>

<div class = "row">
<div class="col-md-6">
<form action="{{url('/addactivitiesinsert')}}" method = "POST" enctype="multipart/form-data"> 
    @csrf
  <div class="mb-3 mt-3">
    <label for="email" class="form-label">Health Id</label>
    <input type="text" class="form-control"  name="healthid" id="healthid"  value = "{{$healthid}}" disabled>
 <input type="hidden" name="id" id="id" value="{{$id}}">
  </div>
  <div class="mb-3">
  <label for="comment">Description:</label>
<textarea class="form-control" rows="5" id="description" name="description"></textarea>
  </div>
  <div class="mb-3 mt-3">
    <label for="file" class="form-label">Document File</label>
  
<div class="addmay" style="margin-top:20px">
    <input type="file" class="form-control" id="document"  name="document[]" style="margin-top:20px"> 
   
</div>
<button class="btn btn-info" id="addnew" type="button" style="margin-top:20px"> Add Document</button>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
<div class="col-md-6"> <img src="{{asset('img/niramayana.jpg')}}" style="margin:120px"> 

</div>
</div> 
</div> 
</section>
</main>

@endsection