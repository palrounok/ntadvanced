<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Activity extends Model
{
    use HasFactory;

    public function user(){

        return $this->belongsTo(User::class);
    }
    public function descriptions()
    {
        return $this->hasMany(Description::class);
    } 
    public function documents()
    {
        return $this->hasMany(Document::class);
    } 
}
