<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Activity;
use App\Models\Document;
use App\Models\Description;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Collection ; // for http client
use Illuminate\Support\Facades\DB ;
use Illuminate\Support\Facades\Storage;

class ActivityController extends Controller
{
    public function index(Request $request){
     $user = Auth::user();
     $id = Auth::user()->id;
     if(isset($request) && $request->search!=''){
        //$activies = Activity::with('user','descriptions','documents')->where('healthid','like','%'.$request->search.'%')->paginate(50) ;
        $activies = Activity::paginate(50)->where('healthid','like','%'.$request->search.'%') ; 
    }else{
     #$activies = Activity::all() ;
     #$activies = Activity::with('user','descriptions','documents')->where('user_id' ,'=' , $id)->get() ;
     $activies = Activity::paginate(50) ;
     #echo "<pre>" ; print_r($activies) ; exit ;

     }

     return view('activities' ,['activities' => $activies , 'user'=>$user]);

    } 
    public function alldataofaclaim($id){
        
        $user = Auth::user();
        //$activities = Activity::with('user','descriptions','documents')->find($id) ;
        $activities = Activity::find($id) ;
        #echo "<pre>" ; print_r($activities->healthid) ; exit ;
        $url = "https://thenationaltrust.gov.in/test_api.php?id=".$activities->healthid ;

        $res = Http::get($url) ;
        $jsonData = $res->json();
        #dd($jsonData) ;

        $desCount = count($activities->descriptions) ; 
        $docCount = count($activities->documents) ; 
        if($desCount  > $docCount){$count = $desCount ;}
        elseif($desCount  < $docCount){$count = $docCount ;}else {$count = $docCount ;}
        return view('alldataofaclaim',['activities' => $activities ,'bdata'=>$jsonData, 'user'=>$user ,'count' => $count,'c'=>1,'id'=>$id]);

    } 
    public function insert(){
        
        $user = Auth::user();
        $id = Auth::user()->id;
        return view('addactivities',['user'=>$user]) ;

    }
    public function insertdata(Request $request){
        //dd($request->id) ;
        //dd($request->file('document')) ; exit ;
        
        //$activities = Activity::where('healthid' ,'=' , $request->healthid)->get()->toArray() ;
       
        
        if($request->id){
            $validated = $request->validate([
                'description' => 'required|max:500',
                'document.*' => 'mimes:jpg,jpeg,pdf,png,doc,docx,zip',
            ]);
           
        $desData = new Description ; 
        $docData = new Document ;
        
        
           // dd($request) ;
            DB::beginTransaction();
            try{
               
                $desData->description = $request->description ; 
                $desData->activity_id  = $request->id ;
                $desData->save() ;
                if($request->hasFile('document')){
                foreach($request->file('document') as $key=>$value) {
                    $filename =time().$request->healthid.$value->getClientoriginalName() ;
                    //echo $filename ; exit ;
                    $value->storeAs('public/document',$filename) ;
                    $insert[$key]['documentpath'] = $filename;
                        $insert[$key]['activity_id'] =  $request->id;
                        $insert[$key]['created_at'] = date('Y-m-d h:i:s') ;
                        $insert[$key]['updated_at'] =   date('Y-m-d h:i:s') ;
                   
                }
                Document::insert($insert);
            }
                DB::commit();
                return redirect('edit/'.$request->id)->with(['msg' => 'Activity is edited successfully']);
            } catch (\Exception $e) {
                DB::rollback();
                // something went wrong
                return redirect('edit/'.$request->id)->withErrors(['msg' => 'Something went wrong!!']);
            }

        }else{
            $validated = $request->validate([
                'healthid' => 'required|unique:activities|size:19|regex:/^[a-zA-Z0-9 ]+$/',
                'description' => 'required|max:500',
                'document.*' => 'mimes:jpg,jpeg,pdf,png,doc,docx,zip',
            ]);
            $mainData = new Activity ;
        $desData = new Description ; 
        $docData = new Document ;
        $mainData->user_id = Auth::user()->id;
        $mainData->healthid = $request->healthid; 
        DB::beginTransaction();
        try{
        $mainData->save() ;
        #ECHO $mainData->id ; EXIT ;
        $desData->description = $request->description ; 
        $desData->activity_id  = $mainData->id ;
        $desData->save() ;
        if($request->hasFile('document')){
        foreach($request->file('document') as $key=>$value) {
            $filename =time().$request->healthid.$value->getClientoriginalName() ;
            //echo $filename ; exit ;
            $value->storeAs('public/document',$filename) ;
            $insert[$key]['documentpath'] = $filename;
                $insert[$key]['activity_id'] =  $mainData->id;
                $insert[$key]['created_at'] = date('Y-m-d h:i:s') ;
                $insert[$key]['updated_at'] =   date('Y-m-d h:i:s') ;
           
        }
        Document::insert($insert);
    }
        DB::commit();
        return redirect('addactivities')->with(['msg' => 'Activity is added successfully']);
    } catch (\Exception $e) {
        DB::rollback();
        // something went wrong
        return redirect('addactivities')->withErrors(['msg' => 'Something went wrong!!']);
    }
}
    }


    public function onedataofaclaim($id){
        //$id = json_encode($id) ;
        $url = "https://thenationaltrust.gov.in/test_api.php?id=".$id ;

        $res = Http::get($url) ;
        $jsonData = $res->json();
        $name = $jsonData['benficiaryname'] ;
       // dd($jsonData) ; exit ;
    
        return redirect('activities')->withErrors(['healthid'=> $id ,'name' => $name, 'state'=>$jsonData['State'] ,'dist'=>$jsonData['District'] ,'ngo'=>$jsonData['Ngoname']]);
    }
    public function edit($id){
        $activities = Activity::with('user','descriptions','documents')->find($id) ;

        $user = Auth::user();
        #$id = Auth::user()->id;
        return view('editactivities',['user'=>$user,'healthid'=>$activities->healthid,'id'=>$id]) ;
    
        //return redirect('activities')->withErrors(['healthid'=> $id ,'name' => $name, 'state'=>$jsonData[1] ,'dist'=>$jsonData[2] ,'ngo'=>$jsonData[3]]);
    }
}
