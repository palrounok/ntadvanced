<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ActivityController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
Route::get('/dashboard',[DashboardController::class,'index'])->middleware(['auth','verified'])->name('dashboard') ;
Route::get('/activities',[ActivityController::class,'index'])->middleware(['auth'])->name('activities') ;
Route::get('/alldataofaclaim/{id}',[ActivityController::class,'alldataofaclaim'])->middleware(['auth'])->name('alldataofaclaim') ;
Route::get('/onedataofaclaim/{id}',[ActivityController::class,'onedataofaclaim'])->middleware(['auth'])->name('onedataofaclaim') ;
Route::get('/addactivities',[ActivityController::class,'insert'])->middleware(['auth'])->name('addactivities') ;
Route::post('/addactivitiesinsert',[ActivityController::class,'insertdata'])->middleware(['auth'])->name('addactivitiesinsert') ;
Route::get('/dashboard1', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');
Route::post('/search',[ActivityController::class,'index'])->middleware(['auth','verified'])->name('search');

Route::get('/edit/{id}',[ActivityController::class,'edit'])->middleware(['auth'])->name('edit') ;


require __DIR__.'/auth.php';
